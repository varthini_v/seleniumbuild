package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;




public class TC001CreateLead extends ProjectMethods {
	
	
	@BeforeTest(groups= {"any"})
	public void setTestCaseName()
	{
		testCaseName="TC001CreateLead";
		testCaseDesc="Create a lead";
		author="varthini";
		category="Smoke";
		workBook="LeafTapsTestData";
		dataSheet=0;
	}
	
	@Test(dataProvider="leaftaps")
	public void CreateLead(String userName,String password,String firstName,String lastName,String CompanyName,String emailID,String PhoneNo)
	{
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.enterCompanyName(CompanyName)
		/*.enterEmailID(emailID)
		.enterPhoneNumber(PhoneNo)*/
		.clickLogin()
		.verifyFirstName(firstName);
		
	}

}
