package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002MergeLead extends ProjectMethods{
	

	@BeforeTest(groups= {"any"})
	public void setTestCaseName()
	{
		testCaseName="TC002MergeLead";
		testCaseDesc="Merge two leads";
		author="varthini";
		category="Smoke";
		workBook="datasample";
		dataSheet=0;
	}
	
	
	@Test(dataProvider="leaftaps")
			//,dependsOnMethods= {"testCases.TC001CreateLead.CreateLead"})
	public void mergeLead(String userName,String password,String fromLead,String toLead)
	{
			new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickMergeLead()
		.clickFirstIcon()
		.enterFirstNameforMergeLead(fromLead)
		.clickFindLeads()
		.clicktTableDataInMergeLead()
		.switchToMegreLeadPage()
		.clickSecondIcon()
		.enterFirstNameforMergeLead(toLead)
		.clickFindLeads()
		.clicktTableDataInMergeLead()
		.switchToMegreLeadPage()
		.clickMergeLead()
		.acceptMergeLeadAlert()
		.verifyFirstName(toLead);
		
		
		
	}

}
