package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods {
	
	public MyLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT,using="Create Lead") WebElement eleCreateLead;
	@FindBy(how=How.LINK_TEXT,using="Merge Leads") WebElement eleMergeLead;
	@FindBy(how=How.LINK_TEXT,using="Find Leads") WebElement eleFindLead;
	@Given("Click on CreateLead link")
	public CreateLeadPage clickCreateLead()
	{
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	public MergeLeadPage clickMergeLead()
	{
		click(eleMergeLead);
		return new MergeLeadPage();
	}
	
	public FindLeadsPage clickFindLead()
	{
		click(eleFindLead);
		return new FindLeadsPage();
	}
}
